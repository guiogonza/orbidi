import requests
import json
from sqlalchemy import create_engine
import pandas as pd
import pymysql


list = [900100953291,900100953296,900100953297]

for ele in list:
    id = ele
    print(id)
    api_token = "pk_3041236_DXOBGO4W2M220TSFKLKVFUDZ50WP4VG9"
    url = f"https://api.clickup.com/api/v2/list/{id}/task"
    headers = {
        "Authorization": api_token
    }

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        data_string = json.dumps(data, indent=4)
    else:
        print("Error en la solicitud: ", response.status_code)


    df = pd.json_normalize(data["tasks"], record_path="linked_tasks", meta=["name","id", "url", "team_id", "time_estimate"])
    df[["producto", "name2"]] = df["name"].str.split(": ", expand=True)

    df["producto"] = df["producto"].str.strip()
    df["name2"] = df["name2"].str.strip()

    proyect_mapping = {
        "KD-WEB": 0,
        "KD-SEO": 1,
        "KD-ANALITICA": 2,
        "KD-RRSS": 3,
        "KD-ECOMMERCE": 4,
        "KD-CRM": 5,
        "KD-PROC": 6,
        "KD-FACT": 7
    }

    df["producto"] = df["producto"].map(proyect_mapping)

    df["time_estimate"] = df["time_estimate"].astype(float)

    #sumatoria_por_producto = df.groupby("proyect")["time_estimate"].sum()
    #print(sumatoria_por_producto)
    #print(df)

    engine = create_engine('mysql+mysqlconnector://guio:guio123**@localhost:3306/orbidi')
    df.to_sql(con=engine, name='proyectos', if_exists='append', index=False)
    engine.dispose()
