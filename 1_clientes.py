import requests
import json
from sqlalchemy import create_engine
import pandas as pd
import pymysql

#list = [900100953154,900100953291,900100953296,900100953297]

id = 900100953154
api_token = "pk_3041236_DXOBGO4W2M220TSFKLKVFUDZ50WP4VG9"

url = f"https://api.clickup.com/api/v2/list/{id}/task"

headers = {
    "Authorization": api_token
}

response = requests.get(url, headers=headers)

if response.status_code == 200:
    data = response.json()
    data_string = json.dumps(data, indent=4)
    print(data_string)
else:
    print("Error en la solicitud: ", response.status_code)

import pandas as pd
df = pd.json_normalize(data["tasks"], record_path="linked_tasks", meta=["name", "id", "url", "team_id"])

print(df)

engine = create_engine('mysql+mysqlconnector://guio:guio123**@localhost:3306/orbidi')
df.to_sql(con=engine, name='clientes', if_exists='append', index=False)
engine.dispose()
